import React from 'react';
import AppRoutes from './routes/AppRoutes';
import './HeroesApp.css';
const HeroesApp = () => {
    return <AppRoutes />;
};

export default HeroesApp;
