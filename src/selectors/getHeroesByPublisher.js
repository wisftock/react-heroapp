import heroes from '../data/heroes';

const getHeroesByPublisher = (publisher) => {
    const validPublishers = ['DC Comics', 'Marvel Comics'];

    // si no encuentra la condicion
    if (!validPublishers.includes(publisher)) {
        throw new Error(`Publishers ${publisher} no es correcto`);
    }

    return heroes.filter((hero) => hero.publisher === publisher);
};

export default getHeroesByPublisher;
