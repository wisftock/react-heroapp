import React, { useMemo } from 'react';
import { useLocation } from 'react-router';
import queryString from 'query-string';
// import heroes from '../../data/heroes';
import useForm from '../../hooks/useForm';
import HeroCard from '../heroes/HeroCard';
import getHeroByName from '../../selectors/getHeroByName';

const SearchScreen = ({ history }) => {
    const location = useLocation();
    // console.log(location.search);
    const { q = '' } = queryString.parse(location.search);

    const [formValues, handleInputChange] = useForm({
        searchText: q,
    });

    const { searchText } = formValues;

    const heroesFiltered = useMemo(() => getHeroByName(q), [q]);
    // const heroesFiltered = getHeroByName(searchText);

    const handleSearch = (e) => {
        e.preventDefault();
        history.push(`?q=${searchText}`);
    };

    return (
        <div>
            <div className='row'>
                <div className='col-5'>
                    <h4>Search Form</h4>
                    <hr />
                    <form onSubmit={handleSearch}>
                        <input
                            type='text'
                            placeholder='Find you hero'
                            className='form-control mb-2'
                            name='searchText'
                            value={searchText}
                            onChange={handleInputChange}
                        />
                        <button
                            type='submit'
                            className='btn btn-primary btn-block'
                        >
                            Search
                        </button>
                    </form>
                </div>
                <div className='col-7'>
                    <h4>Results</h4>
                    {q === '' && (
                        <div className='alert alert-info'>Search a hero</div>
                    )}

                    {q !== '' && heroesFiltered.length === 0 && (
                        <div className='alert alert-danger'>
                            There is no a hero with{q}
                        </div>
                    )}

                    {heroesFiltered.map((hero) => {
                        return <HeroCard key={hero.id} {...hero} />;
                    })}
                </div>
            </div>
        </div>
    );
};

export default SearchScreen;
