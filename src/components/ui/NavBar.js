import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
            <div className='container-fluid'>
                <Link className='navbar-brand' to='/'>
                    Hero
                </Link>
                <button
                    className='navbar-toggler'
                    type='button'
                    data-bs-toggle='collapse'
                    data-bs-target='#navbarText'
                    aria-controls='navbarText'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                >
                    <span className='navbar-toggler-icon'></span>
                </button>
                <div className='collapse navbar-collapse' id='navbarText'>
                    <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
                        <NavLink
                            activeClassName='active'
                            className='nav-item nav-link'
                            exact
                            to='/marvel'
                        >
                            Marvel
                        </NavLink>
                        <NavLink
                            activeClassName='active'
                            className='nav-item nav-link'
                            exact
                            to='/dc'
                        >
                            DC
                        </NavLink>
                        <NavLink
                            activeClassName='active'
                            className='nav-item nav-link'
                            exact
                            to='/search'
                        >
                            Search
                        </NavLink>
                    </ul>
                    <NavLink
                        activeClassName='active'
                        className='nav-item nav-link'
                        exact
                        to='/login'
                    >
                        Logout
                    </NavLink>
                </div>
            </div>
        </nav>
    );
};
export default Navbar;
